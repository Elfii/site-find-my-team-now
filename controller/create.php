<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

include_once ('../model/fonction.php');

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $name_employe = $_POST['name_employe'];
    $first_name = $_POST['first_name'];
    $fonction = $_POST['fonction']; 
    $photo = $_POST['photo']; 
    $name_team = $_POST['name_team'];
    $color_statut = $_POST['color_statut'];
    $id_user = $_POST['id'];

    createEmploye($name_employe, $first_name, $fonction, $photo, $name_team, $color_statut, $id_user);

    header("Location: /MapModulePhp/view/employe.php");
    exit;
}

?>
