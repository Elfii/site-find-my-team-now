<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

include_once ('../model/fonction.php');

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $username = $_POST['username'];
    $pass = password_hash($_POST['pass'], PASSWORD_DEFAULT);
    $email = $_POST['email'];
    
    if(createLogin($username, $pass, $email)){
        header("Location: /MapModulePhp/view/employe.php");
        exit();
    } else {
        // Si la création du client échoue, rediriger vers le formulaire de création avec un message d'erreur
        $_SESSION['erreur_creation_client'] = "Une erreur s'est produite lors de la création du client.";
        header('location: ../formulaire/form_create_login.php');
        exit();
    };

    
}

?>