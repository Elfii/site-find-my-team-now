<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

include_once('../model/fonction.php');

// Vérifier la connexion lorsqu'une requête POST est reçue et si elles ne sont pas vide
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $username = isset($_POST['username']) ? $_POST['username'] : '';
    $pass = isset($_POST['pass']) ? $_POST['pass'] : '';

    // Vérification de connexion
    if (verifyLogin($username, $pass)) {
        exit();
    } else {
        echo "Échec de la connexion. Veuillez vérifier vos informations d'identification.";
    }
}
?>