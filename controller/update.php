<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

include_once ('../model/fonction.php');


if (isset($_POST['id'])) {
    // Ton code pour traiter les données postées
    if ($_SERVER["REQUEST_METHOD"] === "POST") { // verifie si la requête est en POST 
    
        $id_employe = $_POST['id'];
        $name_employe = $_POST['name_employe'];
        $first_name = $_POST['first_name'];
        $fonction = $_POST['fonction'];
        $photo = $_POST['photo'];
        $name_team = $_POST['name_team'];
        $color_statut = $_POST['color_statut'];
    
        // Appeler la fonction updateEmploye() pour effectuer la mise à jour avec l'ID de l'employe
        updateEmploye($id_employe, $name_employe, $first_name, $fonction, $photo, $name_team, $color_statut);
    
        // Rediriger l'utilisateur vers la page read.php après la mise à jour
        header("Location: /MapModulePhp/view/employe.php");
        exit();
    }
}
?>
