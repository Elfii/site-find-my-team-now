DROP DATABASE IF EXISTS maps;
CREATE DATABASE maps;
USE maps;

CREATE TABLE team (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name_team VARCHAR(255) NOT NULL
);

CREATE TABLE employe (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name_employe VARCHAR(255) NOT NULL,
    first_name VARCHAR(255) NOT NULL,
    fonction VARCHAR(255) NOT NULL,
    photo VARCHAR(255) NOT NULL,
    fk_team INT,
    fk_statut INT UNIQUE,
    fk_user INT,
    FOREIGN KEY (fk_team) REFERENCES team (id) ON DELETE CASCADE,
    FOREIGN KEY (fk_statut) REFERENCES statut (id) ON DELETE CASCADE,
    FOREIGN KEY (fk_user) REFERENCES users (id) ON DELETE CASCADE
);

CREATE TABLE statut (
    id INT PRIMARY KEY AUTO_INCREMENT,
    color_statut ENUM('red', 'orange', 'green') NOT NULL
);

CREATE TABLE geolocalisation (
    id INT PRIMARY KEY AUTO_INCREMENT,
    latitude DECIMAL(8,6),
    longitude DECIMAL(9,6),
    timetable TIMESTAMP,
    fk_statut INT,
    FOREIGN KEY (fk_statut) REFERENCES statut (id) ON DELETE CASCADE
);

CREATE TABLE users (
    id INT PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR(255) NOT NULL,
    pass VARCHAR(255) NOT NULL,
    email VARCHAR(170) NOT NULL UNIQUE
);


INSERT INTO team(name_team) VALUES ('équipe 1'), ('équipe 2'), ('équipe 3');

INSERT INTO statut(color_statut) VALUES ('green'),('green'),('green'),('orange'),('red'),('green');

INSERT INTO employe(name_employe, first_name, fonction, photo, fk_team, fk_statut) VALUES
    ('Tessier', 'Gabriel', "Chef d'équipe", 'https://media.istockphoto.com/id/1200677760/fr/photo/verticale-de-jeune-homme-de-sourire-beau-avec-des-bras-crois%C3%A9s.jpg?s=2048x2048&w=is&k=20&c=VP494M_aGswt7FBqah31XF5YVwoAa7bqpYtiPV4-NC8=', 1, 1),  
    ('Le-Breton', 'Yves', 'Ouvrier', 'https://www.bilik.fr/_next/image?url=https%3A%2F%2Fapi.bilik.fr%2Fimages%2F4b58f2421c8d88cc2103a302a4cacf00%2Ffull%2F0%2C0%2F0%2Fdefault.jpg&w=2048&q=75', 1,2),
    ('Cantalou', 'Jean-Hugue ', 'Ouvrier', 'https://www.equans.fr/sites/g/files/tkmtob136/files/styles/coh_x_large/public/2023-05/Adama_Portrait%20RH-carr%C3%A9.jpg?itok=-z7xsrsG', 1, 3),
    ('Dragar','Marcel',"Chef d'équipe",'https://previews.123rf.com/images/alempkht/alempkht1804/alempkht180400654/100399291-jeune-et-bel-homme-posant-devant-le-b%C3%A2timent-sur-lequel-il-travaille.jpg', 2,4),
    ('Tadam','Cambridge',"Ouvrier",'https://media.istockphoto.com/id/1346124870/fr/photo/heureux-travailleur-de-chantier-m%C3%A9tis-regardant-la-cam%C3%A9ra.jpg?s=612x612&w=0&k=20&c=UC3BwJkLMFXIUYmxPdIieiBnc3BNF8X9yCMEJEXtqyQ=', 2,5),
    ('Antoine','Damien',"Ouvrier",'https://www.capcampus.com/img/u/1/job-etudiant-batiment.jpg', 2,6);

INSERT INTO `geolocalisation` (`id`, `latitude`, `longitude`, `timetable`, `fk_statut`) VALUES
(1, '43.266670', '3.083330', '2023-07-28 09:51:42', 1, 1),
(2, '43.266670', '3.008330', '2023-07-28 09:51:42', 2, 1),
(3, '43.200001', '2.766670', '2023-07-28 09:51:42', 3, 1),
(4, '43.266670', '3.008330', '2023-09-14 11:38:24', 4, 1),
(5, '43.483330', '-1.483330', '2023-09-22 20:59:39', 5, 1),
(6, '43.266670', '3.083330', '2023-10-04 13:13:25', 6, 1);

