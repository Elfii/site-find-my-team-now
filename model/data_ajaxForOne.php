<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

include_once ('pdo.php');
include_once ('fonction.php');

$user = $_SESSION['user'];
$id = readUserById($user);

if(isset($_SESSION['user'])) {
    if($id && $id['id'] === $_SESSION['user']) {

        $name_employe = $_POST['name_employe'];


    $stmt = $pdo->prepare(
        "SELECT e.name_employe, e.first_name, e.photo, e.fk_statut, s.id, g.fk_statut, g.latitude, g.longitude, 
        TIME(g.timetable) as timetable 
        FROM employe e 
        INNER JOIN statut s ON e.fk_statut = s.id 
        INNER JOIN geolocalisation g ON s.id = g.fk_statut
        WHERE e.name_employe = :name_employe
        ORDER BY g.timetable 
        DESC LIMIT 10;"); // :name_employe = filtrer les résultats de la requête en fonction d'une valeur spécifique du nom.


    $stmt->bindParam(':name_employe', $name_employe);


    $stmt->execute();

    // Récupérer les résultats de la requête dans un tableau associatif clé/valeur et non index/valeur
    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);


    echo json_encode($results);
    } else{echo "échec";}
        };
?>
