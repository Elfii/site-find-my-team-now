<?php

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

include_once('pdo.php');
include_once ('fonction.php');

$user = $_SESSION['user'];
$id = readUserById($user);

if(isset($_SESSION['user'])) {
    if($id && $id['id'] === $_SESSION['user']) {

$requete = "SELECT e.id, e.name_employe, e.first_name, e.photo, e.fonction, e.fk_statut, s.id, g.latitude, g.longitude, TIME(g.timetable) AS timetable
FROM employe e
INNER JOIN statut s ON e.fk_statut = s.id
INNER JOIN (
    SELECT fk_statut, MAX(timetable) AS last_time
    FROM geolocalisation
    GROUP BY fk_statut
) sub1 ON s.id = sub1.fk_statut
INNER JOIN geolocalisation g ON sub1.fk_statut = g.fk_statut AND sub1.last_time = g.timetable
WHERE s.color_statut = 'green'
GROUP BY e.id;";


// Exécuter la requête avec PDO
$result = $pdo->query($requete);

// Initialiser un tableau vide pour stocker les résultats
$rep = array();

// Parcourir chaque ligne de résultat et ajouter les données au tableau
while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
    $rep[] = $row;
}

// Convertir le tableau des résultats en JSON
echo json_encode($rep, JSON_UNESCAPED_UNICODE);
}} ;
?>

