<?php

include_once ('pdo.php');

function readEmploye($id){
    global $pdo;
    $req = $pdo->prepare("SELECT e.id AS employe_id, e.name_employe, e.first_name, e.fonction, e.photo, e.fk_team, e.fk_statut, e.fk_user, u.id, t.name_team, s.color_statut
        FROM employe e  
        INNER JOIN users u on e.fk_user = u.id
        INNER JOIN team t on e.fk_team = t.id
        INNER JOIN statut s on e.fk_statut = s.id
        WHERE u.id = ?");
    $req->execute([$id]);
    return $req->fetchAll();
}; 


function readEmployeById($id_employe){
    global $pdo;
    $req = $pdo->prepare("SELECT e.id, e.name_employe, e.first_name, e.fonction, e.photo, e.fk_team, e.fk_statut, t.name_team, s.color_statut
        FROM employe e 
        INNER JOIN team t on e.fk_team = t.id
        INNER JOIN statut s on e.fk_statut = s.id
        WHERE e.id = ?");
    $req->execute([$id_employe]);
    return $req->fetchAll();
};


function readEmployeByStatut($id){
    global $pdo;
    $req = $pdo->prepare("SELECT e.id, e.name_employe, e.first_name, e.fk_statut, e.fk_user, u.id, s.color_statut
        FROM employe e 
        INNER JOIN statut s on e.fk_statut = s.id
        INNER JOIN users u on e.fk_user = u.id
        WHERE color_statut = 'green'
        AND e.fk_user = ?");
    $req->execute([$id]);
    return $req->fetchAll();
};

function readTeam() {
    global $pdo;
    $req = $pdo->query("SELECT name_team FROM team");
    return $req->fetchAll();
};



function readStatut($id_employe){
    global $pdo;
    $req = $pdo->prepare("SELECT e.id, e.fk_statut, s.color_statut
    FROM employe e 
    INNER JOIN statut s on e.fk_statut = s.id
    WHERE e.id = ?");
    $req->execute([$id_employe]);
    return $req->fetchAll();
};


function readUserById($user){
    global $pdo;
    $req = $pdo->prepare("SELECT id, username, email FROM users WHERE id=?");
    $req->execute([$user]);
    return $req->fetch(PDO::FETCH_ASSOC);
};


function createEmploye($name_employe, $first_name, $fonction, $photo, $name_team, $color_statut, $id_user) {
    global $pdo;
    try {
        //methode pour démarer une transaction, si toutes sont ok....
        $pdo->beginTransaction();
        $req = $pdo->prepare("INSERT INTO statut (color_statut) VALUES (?)");
        $req->execute([$color_statut]);

        $id_statut = $pdo->lastInsertId();

        $req = $pdo->prepare("INSERT INTO employe (name_employe, first_name, fonction, photo, fk_team, fk_statut, fk_user)
            VALUES (?, ?, ?, ?, (SELECT id FROM team WHERE name_team = ?), ?, ?)");
        $req->execute([$name_employe, $first_name, $fonction, $photo, $name_team, $id_statut, $id_user]);

        $pdo->commit(); //... la requête, la méthode commit() applique les modifications à la base de données.
    } catch (PDOException $e) {
        $pdo->rollBack(); // En cas d'erreur, rollback() annule la transaction 
        echo "Erreur : " . $e->getMessage();
    }
};


function createLogin($username, $pass, $email){
    global $pdo;

    $req = $pdo->prepare("INSERT INTO users (username, pass, email) VALUES (?, ?, ?)");
    $req->execute([$username, $pass, $email]);

    $user = $pdo->lastInsertId();

    if($user){
        $_SESSION['user'] = $user;
        return readUserById($user);
    } else {
        return false;
    }
};


function updateTeam($id_team, $name_team){
    global $pdo;
    $req = $pdo->prepare("UPDATE team SET name_team=? WHERE id=?");
    $req->execute([$name_team, $id_team]);
}



function updateEmploye($id_employe, $name_employe, $first_name, $fonction, $photo, $name_team, $color_statut) {
    global $pdo;

    $updateStatut = $pdo->prepare("UPDATE statut SET color_statut = ? WHERE id = ?");
    $updateStatut->execute([$color_statut, $id_employe]);

    $req = $pdo->prepare("UPDATE employe SET name_employe=?, first_name=?, fonction=?, photo=?, 
        fk_team=(SELECT id FROM team WHERE name_team = ?) WHERE id=?"); 
    $req->execute([$name_employe, $first_name, $fonction, $photo, $name_team, $id_employe]);
}




function createGps($latitude, $longitude, $name_employe){
    global $pdo;

    $req = $pdo->prepare("INSERT INTO geolocalisation(latitude, longitude, timetable, fk_statut)
    SELECT ?, ?, NOW(), s.id
    FROM statut s
    INNER JOIN employe e ON e.fk_statut = s.id
    WHERE e.name_employe = ? AND s.color_statut = 'green';");
    
    $req->execute([$latitude, $longitude, $name_employe]);
}


function verifyLogin($username, $pass) {
    global $pdo;  
    $req = $pdo->prepare("SELECT id, pass FROM users WHERE username = ?");
    $req->execute([$username]);
    $result = $req->fetch(PDO::FETCH_ASSOC);

    // Vérifie si l'utilisateur existe et si le mot de passe est correct
    if ($result && password_verify($pass, $result['pass'])) {
        // Connexion réussie, stockez l'ID de l'utilisateur dans la session
        $_SESSION['user'] = $result['id'];
        header('Location: ../view/index.php');
        exit();}
    else {
        header('Location: /MapModulePhp/view/formulaire/form_login.php?erreur=1');
        exit();
    }
};


function deleteEmploye($id_employe) {
    global $pdo;

    try {
        $pdo->beginTransaction();

        // Supprime les géolocalisations associées à l'employé
        $geo = $pdo->prepare("DELETE FROM geolocalisation 
        WHERE fk_statut = ?");
        $geo->execute([$id_employe]);

        // Supprime le statut associé à l'employé
        $statut = $pdo->prepare("DELETE FROM statut
        WHERE id = ?");
        $statut->execute([$id_employe]);

        // Supprime l'employé
        $employe = $pdo->prepare("DELETE FROM employe 
        WHERE id = ?");
        $employe->execute([$id_employe]);

        $pdo->commit();
    } catch (PDOException $e) {
        $pdo->rollBack();
        echo "Erreur : " . $e->getMessage();
    }
};



?>