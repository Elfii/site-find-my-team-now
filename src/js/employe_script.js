function couleurStatut() {
    let statuts = document.querySelectorAll('.statut');

    statuts.forEach(statutElement => {
        let textElement = statutElement.querySelector('.text');
        let rond = statutElement.querySelector('.rond');
        let couleurStatut = statutElement.getAttribute('data-couleur');

        if (couleurStatut === 'red') {
            textElement.innerText = "Refusé";
            rond.classList.add('rondRouge');
        } else if (couleurStatut === 'orange') {
            textElement.innerText = "NC";
            rond.classList.add('rondOrange');
        } else if (couleurStatut === 'green') {
            textElement.innerText = "Autorisé";
            rond.classList.add('rondVert');
        }
    });
}

couleurStatut();



