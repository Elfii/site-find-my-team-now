var timer;
var map;
var markersLayer;

// Initialisation de la carte et des marqueurs
function initializeMap() {
    var tiles = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        maxZoom: 18,
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors, Points &copy; 2012 LINZ'
    });

    var latlng = L.latLng(43.191669, 3.00833);

    map = L.map('map', {
        center: latlng,
        zoom: 8,
        layers: [tiles]
    });

    markersLayer = L.markerClusterGroup({ disableClusteringAtZoom: 17 });

    createMarkers();
}

// Fonction pour créer les marqueurs
function createMarkers() {
    let ajax = new XMLHttpRequest();
    let method = "POST";
    let url = "../model/data_ajaxLastForAll.php";
    let asynchronous = true;

    ajax.open(method, url, asynchronous);

    ajax.send();

    ajax.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            let datas = JSON.parse(this.responseText);
            console.log(datas);
            // Effacer les anciens marqueurs
            markersLayer.clearLayers();

            datas.forEach(function (data) {
                let lat = parseFloat(data.latitude);
                let long = parseFloat(data.longitude);
                let first_name = data.first_name;
                let name = data.name_employe;
                let timetable = data.timetable;
                let marker = L.marker(L.latLng(lat, long));

                // Créer le contenu de la popup du marqueur
                var customPopup = document.createElement('div');
                customPopup.className = 'popup';
                customPopup.innerHTML = `<h2>${name}</h2><h3>${first_name}</h3><br/><h3>${timetable}</h3><img src='${data.photo}' width='100%'><br/><br/>`;

                marker.bindPopup(customPopup).openPopup();
                markersLayer.addLayer(marker);

                marker.on('mouseover', function () {
                    marker.openPopup();
                });

                addPopupClickEvent(customPopup,name);
            });

            // Ajouter la couche de regroupement de marqueurs à la carte
            map.addLayer(markersLayer);
        }
    };
}

// Fonction pour ajouter un événement de clic à la popup du marqueur
function addPopupClickEvent(customPopup, nom) {
    customPopup.addEventListener('click', function () {
        fetch('../model/data_ajaxForOne.php', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            },
            body: 'name_employe=' + encodeURIComponent(nom),
        })
            .then(response => {
                if (!response.ok) {
                    throw new Error(`Erreur de réseau: ${response.status}`);
                }
                return response.json();
            })
            .then(json_data => {
                // Arrêter le timer pour éviter les mises à jour automatiques pendant la visualisation des positions
                stopTimer();

                // Effacer les anciens marqueurs
                markersLayer.clearLayers();

                // Parcourir les données récupérées pour créer les nouveaux marqueurs sur la carte
                json_data.forEach(records => {
                    // Créer un marqueur à la position correspondante
                    let markers = L.marker([records.latitude, records.longitude]).addTo(markersLayer);

                    // Créer une nouvelle popup avec les informations de l'utilisateur et de la position
                    var customPopups = document.createElement('div');
                    customPopups.className = 'popup';
                    customPopups.innerHTML = `<h2>${records.name_employe}</h2><h3>${records.first_name}</h3><br/><h3>${records.timetable}</h3><br/><img src='${records.photo}' width='100%'><br/><br/>`;
                    markers.bindPopup(customPopups).openPopup();
                    markersLayer.addLayer(markers);
                });

                // Ajouter la couche de regroupement de marqueurs à la carte
                map.addLayer(markersLayer);
            })
            .catch(error => {
                console.error(error);
            });
    });
};


// Créer une fonction pour initialiser le timer
function startTimer() {
    timer = setInterval(createMarkers, 5000);
    console.log('Start réussi');
};

// Fonction pour arrêter le timer
function stopTimer() {
    clearInterval(timer);
    console.log('Clear réussi');
};

// Créer le bouton de réinitialisation + événement de rechargement des marqueurs
function reloadTimerClick() {
    let button = document.createElement('button');
    button.innerText = 'Actualiser';
    button.classList.add('boutton');
    button.addEventListener('click', function () {
        createMarkers();
        startTimer();
    });
    

    let container = document.getElementById('button_container');

    // Effacer le contenu existant avant d'ajouter le nouveau bouton
    container.innerHTML = '';
    
    // Ajouter le bouton au conteneur
    container.appendChild(button);
    
    console.log('Reload réussi');
}


// Initialiser la carte au chargement de la page
initializeMap();

// Lancer le timer pour obtenir les marqueurs
startTimer();

// Lancer la fonction pour créer le bouton de réinitialisation + événement de rechargement des marqueurs
reloadTimerClick();
