let button = document.getElementById('js_menu');
let menu = document.getElementsByClassName('nav_menu');
let display = false;

button.addEventListener('click', () => {

    // stopTimer();

    display = !display; // Inverser la valeur de display

    Array.from(menu).forEach((item) => {
        if (display) {
            item.style.display = 'flex';
            item.style.flexDirection = 'column';
            item.style.alignContent = 'center';
            item.style.justifyContent = 'center';
        } else {
            item.style.display = 'none';
        }
    });
});







