<?php 
    include_once '../debug.php';
    include_once './template.php';
    include_once '../model/fonction.php';

    if(isset($_SESSION['user'])) {
        $user = $_SESSION['user'];
        $id = readUserById($user);
        if($id && $id['id'] === $_SESSION['user']) {
            $employes = readEmploye($id['id']);
            if(empty($employes)){ ?>
                <div class="container_session">
                <div class="session">
                    <h1>Votre espace est vide</h1>
                    <h2>Merci d'enregistrer des données!</h2>
                </div>
            </div> 
            <?php } ?>
    <div class="container_employe">
    <?php
        foreach($employes as $employe){ ?>
                <div class="card">

                    <div class="info">
                        <img class="photo"src="<?= $employe['photo'];?>" alt="Photo de l'employé">
                        <h2><?= $employe['employe_id'];?> - <?= $employe['name_employe'];?> <?= $employe['first_name'];?> </h2> 
                            <div class="fonction">
                                <h3 class="titre_employe">Equipe :</h3>
                                <p><?= $employe['name_team'];?></p>
                                <h4 class="titre_employe">Fonction :</h4>
                                <p><?= $employe['fonction'];?></p>
                            </div>  
                    </div>

                    
                    <div class="statut" data-couleur="<?= $employe['color_statut']?>">
                        <div class="statut_text"> 
                            <p class="text"></p>
                        </div>
                        <div class="rond"></div>
                        <div class="button_update">
                        <input class="update" type="button" onclick="document.location.href='/MapModulePhp/view/formulaire/form_update_employe.php?id=<?= $employe['employe_id']; ?>';">
                        </div>
                    </div>

                </div>
    <?php }}} else { ?>
            <div class="container_session">
                <div class="session">
                    <h1>Vous n'avez pas accés à ces données</h1>
                    <h2>Merci de vous connecter!</h2>
                </div>
            </div> 
        <?php } ?>
        <div class="container_button">
            <input class="button" type="button" value="Ajouter un employé" onclick="document.location.href='/MapModulePhp/view/formulaire/form_create_employe.php';">
            <input class="button" type="button" value="Ajouter point gps" onclick="document.location.href='/MapModulePhp/view/formulaire/form_create_gps.php';">
            <input class="button" type="button" value="Supprimer un employé" onclick="document.location.href='/MapModulePhp/view/formulaire/form_delete_employe.php';">
        </div>
    </div>

<script src="../src/js/employe_script.js"></script>