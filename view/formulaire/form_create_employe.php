<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

include_once ('../../model/fonction.php');
include_once ('../template.php');

if(isset($_SESSION['user'])) {
    $user = $_SESSION['user'];
    $id = readUserById($user);
    if($id && $id['id'] === $_SESSION['user']) {
        $id_user = $id['id']; ?>
<div class="container_formulaire">
    <div class="container_form">
        <form class="form" action="../../controller/create.php" method="post">
            <label for="name">Nom :</label>
            <input type="text" name="name_employe"></br></br>
            <label for="name">Prenom :</label>
            <input type="text" name="first_name"></br></br>
            <label for="name">Fonction dans l'équipe :</label>
            <input type="text" name="fonction"></br></br>
            <label for="name">URL photo :</label>
            <input type="url" name="photo"></br></br>
            <label for="name">Nom de l'équipe :</label>
            <select name="name_team" required>
                <?php
                // Récupérer la liste des teams depuis la base de données
                $teams = readTeam();

                // Afficher chaque team dans la liste déroulante
                foreach ($teams as $team) {
                echo "<option value=\"$team[name_team]\">$team[name_team]</option>";
                }
                ?>
            </select>

            <label for="name">Autorisation :</label>
            <select name="color_statut">
                    <option value="red">rouge</option>
                    <option value="orange">orange</option>
                    <option value="green">vert</option>
                </select>
                <input type="hidden" name="id" value="<?php echo $id_user; ?>">
            <input class="button" type="submit" value="Valider">
        </form>
    </div>
</div>
<?php }} else {?>
            <div class="container_session">
                <div class="session">
                    <h1>Vous n'avez pas accés à ces données</h1>
                    <h2>Merci de vous connecter!</h2>
                </div>
            </div> <?php } ?>



