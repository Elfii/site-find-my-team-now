<?php
include_once ('../../model/fonction.php');
include_once ('../template.php');

if(isset($_SESSION['user'])) {
    $user = $_SESSION['user'];
    $id = readUserById($user);
    if($id && $id['id'] === $_SESSION['user']) {
        $employes = readEmployeByStatut($id['id']);?>

    <div class="container_formulaire">
        <div class="container_form">
            <form class="form" action="../../controller/create_gps.php" method="post">
                <label for="latitude">Latitude :</label>
                <input type="text" name="latitude" required></br></br>
                
                <label for="longitude">Longitude :</label>
                <input type="text" name="longitude" required></br></br>
                
                <label for="name_employe">Nom de l'employé :</label>
                <select name="name_employe" required>
                    <?php
                    // Récupérer la liste des employés depuis la base de données
                        $employes ;

                    // Afficher chaque employé dans la liste déroulante
                    foreach ($employes as $employe) {
                        echo "<option value='" . $employe['name_employe'] . "'>" . $employe['name_employe'] . "</option>";
                    }
                    ?>
                </select>

                <input class="button" type="submit" value="Valider">
            </form>
        </div>
    </div>
<?php }} else {?>
        <div class="container_session">
            <div class="session">
                <h1>Vous n'avez pas accés à ces données</h1>
                <h2>Merci de vous connecter!</h2>
            </div>
        </div> <?php } ?>
