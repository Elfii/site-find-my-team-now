<?php
include_once ('../../model/fonction.php');
include_once ('../template.php');

// Vérifier si la clé d'erreur existe et n'est pas vide
if (isset($_SESSION['erreur_creation_client']) && !empty($_SESSION['erreur_creation_client'])) {
    echo "<p style='color: red;'>Erreur : " . $_SESSION['erreur_creation_client'] . "</p>";
    // Effacer la variable de session après l'avoir affichée
    unset($_SESSION['erreur_creation_client']);
};

?>
<div class="container_formulaire">
    <div class="container_form">
        <form class="form" action="../../controller/create_login.php" method="post">
            <label for="username">Nom :</label>
            <input type="text" name="username"></br></br>
            <label for="pass">Mot de passe :</label>
            <input type="text" name="pass"></br></br>
            <label for="email">Mail :</label>
            <input type="text" name="email"></br></br>
            <input class="button" type="submit" value="Valider">
        </form>
    </div>
</div>

