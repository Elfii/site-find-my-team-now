<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
};

include_once ('../../model/fonction.php');
include_once ('../template.php');


if(isset($_SESSION['user'])) {
    $user = $_SESSION['user'];
    $id = readUserById($user);
    if($id && $id['id'] === $_SESSION['user']) {
            $employes = readEmploye($id['id']);
    ?>
        <div class="container_formulaire">
            <div class="container_form">
            <form class="form" action="../../controller/delete.php" method="post">
                <label for="id">Sélectionnez l'employé à supprimer :</label>
                <select name="employe_id">
                    <?php foreach ($employes as $employe): ?>
                        <option value="<?php echo $employe['employe_id']; ?>">
                            <?php echo $employe['name_employe'] . ' ' . $employe['first_name']; ?>
                        </option>
                    <?php endforeach; ?>
                </select>

                <input class="button" type="submit" value="Valider">
            </form>
            </div>
        </div>
<?php }} else {?>
    <div class="container_session">
        <div class="session">
            <h1>Vous n'avez pas accés à ces données</h1>
            <h2>Merci de vous connecter!</h2>
        </div>
    </div> <?php } ?>

