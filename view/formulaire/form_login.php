<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

include_once ('../../model/fonction.php');
include_once ('../template.php');
include_once('../../debug.php');
?>

<div class="container_formulaire">
    <div class="container_form">
        <form class="form" action="/MapModulePhp/controller/log_in.php" method="post">
        <h1>Connexion</h1>

        <label><b>Nom d'utilisateur</b></label>
        <input type="text" placeholder="Entrer le nom d'utilisateur" name="username" required>

        <label><b>Mot de passe</b></label>
        <input type="pass" placeholder="Entrer le mot de passe" name="pass" required>

        <input type="submit" id='submit' value='Valider'>
            <?php
                if(isset($_GET['erreur'])){
                    $err = $_GET['erreur'];
                if($err==1 || $err==2)
                    echo "<p style='color:red'>Utilisateur ou mot de passe incorrect</p>";
                };
            ?>
        <a href="../formulaire/form_create_login.php">Pas encore inscrit?</a>
        </form>
    </div>
</div>