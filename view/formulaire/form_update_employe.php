<?php
include_once ('../../model/fonction.php');
include_once ('../template.php');

if(isset($_SESSION['user'])) {
    $user = $_SESSION['user'];
    $id = readUserById($user);
    if($id && $id['id'] === $_SESSION['user']) {
        // Vérifiez si l'ID de l'employé est défini dans l'URL
        if (isset($_GET['id'])) {
            // Récupérez l'ID de l'employé depuis l'URL
            $id_employe = $_GET['id'];

            // Appelez la fonction readEmploye() avec l'ID de l'employé pour récupérer ses informations
            $employe = readEmployeById($id_employe);
            var_dump($id_employe);
            foreach ($employe as $info) {
        ?>
        <div class="container_formulaire">
            <div class="container_form">
                <form class="form" action="../../controller/update.php" method="post">
                    <label for="name">Nom :</label>
                    <input type="text" name="name_employe" value="<?php echo $info['name_employe']; ?>"></br>
                    <label for="name">Prenom :</label>
                    <input type="text" name="first_name" value="<?php echo $info['first_name']; ?>"></br>
                    <input type="hidden" name="id" value="<?php echo $info['id']; ?>"></br>
                    <label for="name">Fonction dans l'équipe :</label>
                    <select name="fonction"></br>
                        <option value="Chef d'équipe">Chef d'équipe</option>
                        <option value="Ouvrier">Ouvrier</option>
                    </select></br></br>
                    <label for="name">URL photo :</label>
                    <input type="url" name="photo" value="<?php echo $info['photo']; ?>"></br></br>
                    <label for="name">Nom de l'équipe :</label>
                    <input type="text" name="name_team" value="<?php echo $info['name_team']; ?>"></br></br>
                    <label for="name">Autorisation :</label>
                    <select name="color_statut"></br></br>
                        <option value="red">rouge</option>
                        <option value="orange">orange</option>
                        <option value="green">vert</option>
                    </select>
                    <input class="button" type="submit" value="Valider">
                </form>
            </div>
        </div>
        <?php

            }
        } else {
            echo "L'ID de l'employé n'est pas défini dans l'URL.";
        };
    }}
?>
