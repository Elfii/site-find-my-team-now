<?php
    include_once ('./template.php');
?>

<div class="container_index">

    <div class="container_video">
        <div class="background_video">
            <video autoplay loop muted>
                <source src="../image/video/background.mp4" type="video/mp4">
            </video>
        </div>
    </div>
    
</div>
