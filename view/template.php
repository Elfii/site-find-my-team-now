<?php if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/MapModulePhp/src/css/style.css">
    <title>FindMyTeamNow()</title>

</head>
<body>

<div class="container_header">
        <div class="logo_container">
            <a href="/MapModulePhp/view/index.php"><div class="logo"></div></a>
        </div>   

    <div class="nom_container">
        <div class="nav_menu">
            <nav>
                <div class="a_menu"><a href="/MapModulePhp/view/index.php"> Accueil</a></div></br>
                <div class="a_menu"><a href="/MapModulePhp/view/employe.php">Équipes</a></div>
            </nav>
        </div>
            <p id="nom_site">Find My Team Now<span id="signe">()</span></p>
        <div class="nav_menu">
            <nav>
                <div class="a_menu"><a href="/MapModulePhp/view/map.php">Carte</a></div></br>
                <div class="a_menu"><a href="/MapModulePhp/view/contact.php">Contact</a></div>
            </nav>
        </div>
    </div>

    <div class="menu">
        <button id="js_menu" class="size_image"></button>
        <nav>
            <div class="a_menu"><a href="/MapModulePhp/view/formulaire/form_login.php">Log-in</a></div></br>
            <div class="a_menu"><a href="/MapModulePhp/controller/log_out.php">Log-out</a></div>
        </nav>
    </div>
</div>
    

<script src="/MapModulePhp/src/js/menu_script.js"></script>

</body>
</html>